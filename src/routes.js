import Home from './components/Home';
import Categoria from './components/Categoria';
import Productos from './components/Productos';
import Informacion from './components/Informacion';
import SiteMap from './components/SiteMap';

const routes = [
	{ path: '/seo-test/', component: Home, name: 'home'},
	{ path: '/seo-test/categoria', component: Categoria, name: 'categoria' },
	{ path: '/seo-test/productos', component: Productos, name: 'productos' },
	{ path: '/seo-test/informacion', component: Informacion, name: 'informacion' },
	{ path: '/seo-test/sitemap', component: SiteMap },
];

export default routes;