import VueRouterSitemap      from 'vue-router-sitemap';
import path                  from 'path';
import { routes } from "./routes.js";
 

const sitemapMiddleware = () => {
  return (req, res) => {

  	console.log(req);

    res.set('Content-Type', 'application/xml');
 
    const staticSitemap = path.resolve('dist/static', 'sitemap.xml');
    const filterConfig = {
      isValid: false,
      rules: [
        /\/example-page/,
        /\*/,
      ],
    };
 
    new VueRouterSitemap(routes).filterPaths(filterConfig).build('http://example.com').save(staticSitemap);
 
    return res.sendFile(staticSitemap);
  };
};

export default sitemapMiddleware;
 
// get('/sitemap.xml', sitemapMiddleware());